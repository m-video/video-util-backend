package com.kyyblabla.videoutil.parser;

import com.kyyblabla.videoutil.enums.VideoType;
import com.kyyblabla.videoutil.exception.VideoParseException;
import com.kyyblabla.videoutil.vo.VideoInfoVo;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author kyy
 * @date 2019/06/20
 */
@Slf4j
public abstract class VideoParser {

    protected static final String PC_UA = "Mozilla/5.0 (Linux; Android 8.0.0; MI 6 Build/OPR1.170623.027; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/62.0.3202.84 Mobile Safari/537.36";
    protected static final String PHONE_UA = "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1";


    public VideoInfoVo parse(String url) {
        log.info("准备解析链接:{}" , url);
        try {
            VideoInfoVo videoInfoVo = onParse(url);
            videoInfoVo.setType(videoType().name);
            videoInfoVo.setShareUrl(url);
            // TODO 存储解析到的数据
            videoInfoVo.setResponse("");
            log.info("通过链接{}解析到视频信息:{}" , url, videoInfoVo.toString());
            return videoInfoVo;
        } catch (IOException e) {
            // 平台url访问失败
            log.error("视频平台访问错误", e);
            throw new VideoParseException("视频平台访问错误" , e);
        } catch (Exception e) {
            log.error("视频信息解析错误", e);
            // 内容解析失败或其他未知错误
            throw new VideoParseException("视频信息解析错误" , e);
        }

    }

    abstract protected VideoInfoVo onParse(String url) throws IOException;

    abstract protected VideoType videoType();


}
