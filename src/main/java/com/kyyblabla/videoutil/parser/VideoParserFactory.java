package com.kyyblabla.videoutil.parser;

import com.kyyblabla.videoutil.enums.VideoType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

/**
 * @author kyy
 * @date 2019/06/21
 */
@Component
@Slf4j
public class VideoParserFactory {


    private static VideoParser douyinParser = new DouyinParser();
    private static VideoParser kuaishouParser = new KuaishouParser();
    private static VideoParser weishiParser = new WeishiParser();

    public VideoParser getVideoParserFromUrl(String shareUrl) {
        VideoType videoType = getTypeFormUrl(shareUrl);
        return getVideoParser(videoType);
    }

    public VideoParser getVideoParser(VideoType videoType) {
        if (VideoType.DOUYIN.equals(videoType)) {
            return douyinParser;
        } else if (VideoType.KUAISHOU.equals(videoType)) {
            return kuaishouParser;
        } else if (VideoType.WEISHI.equals(videoType)) {
            return weishiParser;
        }
        throw new RuntimeException("暂不支持该链接解析，敬请期待");
    }

    private VideoType getTypeFormUrl(String url) {
        String host;
        try {
            host = new URL(url).getHost();
        } catch (MalformedURLException e) {
            String info = "错误的链接";
            log.error(info);
            throw new RuntimeException(info, e);
        }
        return Arrays.stream(VideoType.values())
                .filter(videoType -> StringUtils.contains(host, videoType.host))
                .findAny().orElseThrow(() -> new RuntimeException("不支持解析的链接"));
    }


}
