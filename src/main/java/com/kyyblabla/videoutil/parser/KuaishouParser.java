package com.kyyblabla.videoutil.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kyyblabla.videoutil.enums.VideoType;
import com.kyyblabla.videoutil.vo.VideoInfoVo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * @author kyy
 * @date 2019/06/21
 */
public class KuaishouParser extends VideoParser {

    /**
     * resp := HttpGet(url, pc_ua)
     * photoId := regexp.MustCompile(`"photoId":"(.*?)",`).FindAllStringSubmatch(resp, -1)
     * if len(photoId) < 1 || len(photoId[0]) < 2 {
     * return Echo(400, "参数错误", nil)
     * }
     * resp = HttpGet("https://api.kmovie.gifshow.com/rest/n/kmovie/app/photo/getPhotoById?WS&jjh_yqc&ws&photoId="+photoId[0][1], pc_ua)
     * respJson := gjson.Parse(resp)
     * text := respJson.Get("photo.caption").Str
     * cover := respJson.Get("photo.coverUrl").Str
     * playAddr := respJson.Get("photo.mainUrl").Str
     * if playAddr == "" {
     * return Echo(400, "解析错误", nil)
     * }
     * echoMap := make(map[string]interface{})
     * echoMap["text"] = text
     * echoMap["cover"] = cover
     * echoMap["playAddr"] = playAddr
     * return Echo(200, "", echoMap)
     **/

    protected static final String API = "http://api.gifshow.com/rest/n/photo/info?";


    @Override
    protected VideoInfoVo onParse(String url) throws IOException {

        Document document = Jsoup.connect(url).header("user-agent", PC_UA).get();
        String pagedata = document.getElementById("hide-pagedata").attr("data-pagedata");
        String photoId = JSON.parseObject(pagedata).getString("photoId");

        String params = "client_key=56c3713c&photoIds=" + photoId;
        String sig = md5m(params.replaceAll("&", "") + "23caab00356c");
        document = Jsoup.connect(API + params + "&sig=" + sig)
                .ignoreContentType(true)
                .header("user-agent", PC_UA)
                .get();

        JSONObject jsonObject = JSON.parseObject(document.text());
        JSONObject photo = jsonObject.getJSONArray("photos").getJSONObject(0);
        if (photo == null) {
            throw new RuntimeException("无法解析图集");
        }
        String caption = photo.getString("caption");
        String coverUrl = photo.getString("thumbnail_url");
        String mainUrl = photo.getString("main_mv_url");
        VideoInfoVo videoInfoVo = new VideoInfoVo();
        videoInfoVo.setPlayAddr(mainUrl);
        videoInfoVo.setText(caption);
        videoInfoVo.setMusic("");
        videoInfoVo.setCover(coverUrl);
        videoInfoVo.setResponse(document.text());
        return videoInfoVo;
    }

    @Override
    protected VideoType videoType() {
        return VideoType.KUAISHOU;
    }

    private String md5m(String data) {
        try {
            // 生成一个MD5加密计算摘要
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 计算md5函数
            md.update(data.getBytes());
            // digest()最后确定返回md5 hash值，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            return new BigInteger(1, md.digest()).toString(16);
        } catch (Exception e) {
            throw new RuntimeException("MD5加密出现错误");
        }
    }

    public static void main(String[] args) {

        //@正义网 发了一个快手作品，一起来看！暖心！全国多所高校为湖北籍学生发补助金 https://kphbeijing.m.chenzhongtech.com/s/1S2ez25E 复制此链接，打开【快手App】直接观看！
        VideoInfoVo parse = new KuaishouParser().parse("https://kphbeijing.m.chenzhongtech.com/s/1S2ez25E");
        System.out.println(parse);

    }

}
