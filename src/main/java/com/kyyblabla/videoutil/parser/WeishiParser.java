package com.kyyblabla.videoutil.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kyyblabla.videoutil.enums.VideoType;
import com.kyyblabla.videoutil.vo.VideoInfoVo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

/**
 * @author kyy
 * @date 2019/06/21
 */
public class WeishiParser extends VideoParser {


    protected static final String API = "https://h5.qzone.qq.com/webapp/json/weishi/WSH5GetPlayPage?feedid=";

    /*
    feedid := regexp.MustCompile(`feed/(\w+)`).FindAllStringSubmatch(url, -1)
	if len(feedid) < 1 || len(feedid[0]) < 2 {
		return Echo(400, "参数错误", nil)
	}
	resp := HttpGet("https://h5.qzone.qq.com/webapp/json/weishi/WSH5GetPlayPage?feedid="+feedid[0][1], pc_ua)
	respJson := gjson.Parse(resp)
	if respJson.Get("data.feeds.0.video_url").String() == "" || respJson.Get("data.feeds.0.images.0.url").String() == "" {
		return Echo(400, respJson.Get("data.errmsg").String(), nil)
	}
	echoMap := make(map[string]interface{})
	echoMap["text"] = respJson.Get("data.feeds.0.feed_desc").String()
	echoMap["cover"] = respJson.Get("data.feeds.0.images.0.url").String()
	echoMap["playAddr"] = respJson.Get("data.feeds.0.video_url").String()
	return Echo(200, "", echoMap)
    */

    Pattern r = compile("feed/(\\w+)");

    @Override
    protected VideoInfoVo onParse(String url) throws IOException {

        Matcher m = r.matcher(url);
        Assert.isTrue(m.find(), "无法解析的链接");
        String feedId = m.group(1);

        Document document = Jsoup.connect(API + feedId).ignoreContentType(true).header("user-agent", PC_UA).get();

        JSONObject jsonObject = JSON.parseObject(document.text());
        JSONObject data = jsonObject.getJSONObject("data");
        if (data == null) {
            throw new RuntimeException("无法解析结果");
        }
        JSONObject feed0 = data.getJSONArray("feeds").getJSONObject(0);
        if (feed0 == null) {
            throw new RuntimeException("无法解析结果");
        }

        String feed_desc = feed0.getString("feed_desc");
        String coverUrl = feed0.getJSONArray("images").getJSONObject(0).getString("url");
        String video_url = feed0.getString("video_url");
        VideoInfoVo videoInfoVo = new VideoInfoVo();
        videoInfoVo.setPlayAddr(video_url);
        videoInfoVo.setText(feed_desc);
        videoInfoVo.setCover(coverUrl);
        videoInfoVo.setResponse(document.text());
        return videoInfoVo;
    }

    @Override
    protected VideoType videoType() {
        return VideoType.WEISHI;
    }

    public static void main(String[] args) {

        VideoInfoVo parse = new WeishiParser().parse("https://h5.weishi.qq.com/weishi/feed/77aW6tVkW1HBGNhlY/wsfeed?_proxy=1&_wv=1&wxplay=1&id=77aW6tVkW1HBGNhlY&spid=7277539328447352858&reqseq=893509913&cover=http%3A%2F%2Fxp.qpic.cn%2Foscar_pic%2F0%2F1047_39393661383061642d323331392pict%2F0&bgSize=cover&image=1047_39393661383061642d323331392&xflag=2130706433a1685188656b1561094059");
        System.out.println(parse);
    }

}
