package com.kyyblabla.videoutil.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kyyblabla.videoutil.enums.VideoType;
import com.kyyblabla.videoutil.exception.VideoParseException;
import com.kyyblabla.videoutil.vo.VideoInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.util.Assert;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;


/**
 * @author kyy
 * @date 2019/06/19
 */
@Slf4j
public class DouyinParser extends VideoParser {

    protected static final String API = "https://www.iesdouyin.com/web/api/v2/aweme/iteminfo/?item_ids=%s&dytk=%s";

    Pattern REG_OF_ITEM_ID = compile("(itemId: \"(\\d+)\")");
    Pattern REG_OF_DYTK = compile("(dytk: \"([^\"]+)\")");

    @Override
    protected VideoInfoVo onParse(String url) throws IOException {
        Document document = Jsoup.connect(url).header("user-agent", PC_UA).timeout(5000).get();

        String html = document.toString().replace("\n", "");
        html = html.replaceAll("\r", "");
        // 创建 Pattern 对象
        // 现在创建 matcher 对象
        Matcher m = REG_OF_ITEM_ID.matcher(html);
        Assert.isTrue(m.find(), "解析失败");

        String itemId = getValue(html, REG_OF_ITEM_ID);
        String dytk = getValue(html, REG_OF_DYTK);
        log.info("parse from url:【{}】 get itemId:[{}], dytk:[{}]", url, itemId, dytk);

        document = Jsoup.connect(String.format(API, itemId, dytk))
                .ignoreContentType(true)
                .header("user-agent", PHONE_UA).timeout(5000).get();

        String json = document.text();
        log.info("获取结果：{}", json);
        JSONObject jsonObject = JSON.parseObject(json);
        JSONObject videoDetail = (JSONObject) jsonObject.getJSONArray("item_list").get(0);

        String playAddr = videoDetail
                .getJSONObject("video").getJSONObject("play_addr").getJSONArray("url_list").getString(0);

        playAddr = getLocationUrl(playAddr);

        if (StringUtils.isBlank(playAddr)) {
            throw new VideoParseException("无法获取到视频地址");
        }
        String shareTitle = videoDetail.getString("desc");
        String cover = videoDetail.getJSONObject("video").getJSONObject("cover").getJSONArray("url_list").getString(0);
        String music = "";
        VideoInfoVo videoInfoVo = new VideoInfoVo();
        videoInfoVo.setPlayAddr(playAddr);
        videoInfoVo.setText(shareTitle);
        videoInfoVo.setMusic(music);
        videoInfoVo.setCover(cover);
        videoInfoVo.setResponse(document.text());
        return videoInfoVo;

    }

    @Override
    protected VideoType videoType() {
        return VideoType.DOUYIN;
    }

    public static String getLocationUrl(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        //得到connection对象。
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        //设置请求方式
        connection.setRequestMethod("GET");
        connection.addRequestProperty("user-agent", PHONE_UA);
        //连接
        connection.connect();
        //得到响应码
        Map<String, List<String>> headerFields = connection.getHeaderFields();
        List<String> locations = headerFields.get("location");
        if (locations.isEmpty()) {
            return null;
        } else {
            return locations.get(0);
        }
    }

    private String getValue(String html, Pattern p) {
        Matcher m = p.matcher(html);
        Assert.isTrue(m.find(), "解析失败");
        String value = m.group(2);
        return value;
    }

    public static void main(String[] args) throws IOException {

//        new DouyinParser().parse("https://v.douyin.com/qDLfVt/");
        String locationUrl = getLocationUrl("https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fe90000bo9a5k4ttc5ru9km6br0&line=0&ratio=540p&media_type=4&vr_type=0&improve_bitrate=0&is_play_url=1&is_support_h265=0&source=PackSourceEnum_PUBLISH");
        System.out.println(locationUrl);
    }

}
