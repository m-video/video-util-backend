package com.kyyblabla.videoutil.exception;

/**
 * @author kyy
 * @date 2019/06/21
 */
public class VideoParseException extends RuntimeException {

    public VideoParseException(String message) {
        super(message);
    }

    public VideoParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
