package com.kyyblabla.videoutil.controller;

import com.kyyblabla.videoutil.parser.VideoParserFactory;
import com.kyyblabla.videoutil.vo.VideoInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author kyy
 * @date 2019/06/20
 */
@RequestMapping("/api/video")
@RestController
@CrossOrigin
public class VideoParseController {


    @Autowired
    private VideoParserFactory videoParserFactory;

    @GetMapping
    public VideoInfoVo parse(@RequestParam("shareUrl") String shareUrl) {
        return videoParserFactory.getVideoParserFromUrl(shareUrl).parse(shareUrl);
    }

}
