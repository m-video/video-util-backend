package com.kyyblabla.videoutil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoUtilApplication {


    public static void main(String[] args) {
        SpringApplication.run(VideoUtilApplication.class, args);
    }

}
