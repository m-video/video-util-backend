package com.kyyblabla.videoutil.vo;

import lombok.Data;

/**
 * @author kyy
 * @date 2019/06/20
 */
@Data
public class VideoInfoVo {
    private String playAddr;
    private String music;
    private String cover;
    private String text;
    private String shareUrl;
    private String type;
    private String response;
}
