package com.kyyblabla.videoutil.enums;

/**
 * @author kyy
 * @date 2019/06/20
 */
public enum VideoType {

    DOUYIN("抖音", "v.douyin.com"),
    KUAISHOU("快手", "m.chenzhongtech.com"),
    WEISHI("微视", "h5.weishi.qq.com"),
    ;

    public String name;
    public String host;

    VideoType(String name, String host) {
        this.name = name;
        this.host = host;
    }
}
