mvn clean  package spring-boot:repackage -Dmaven.test.skip=true

function updateBuildNo() {
   build_no=1
   if [ -f './build_no' ]
   then
     build_no=`cat ./build_no`
   fi
   echo $[$build_no+1] > "./build_no"
   echo  ${build_no}
}
build_no=$(updateBuildNo)

cd './target'
cat > "Dockerfile"  << EOF

FROM insideo/jre8

EXPOSE 8085

VOLUME /tmp/logs/video-util

COPY video-util.jar app.jar

ENTRYPOINT ["java","-Dfile.encoding=UTF-8","-jar","app.jar"]

EOF

image_name="registry.cn-beijing.aliyuncs.com/gegewu/vu:${build_no}"

docker build -t ${image_name} .
docker push ${image_name}

#key/secret
#3D72BFFA5BE580ED8535
#zvCRmNynTvwsueNsFUDDTgfbC6auEsuob3F7Zana